// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.


export const environment = {
  production: false,
  // FURNICLOUD_URL: 'http://cloud.furnitalia.com/',
  FURNICLOUD_URL: 'http://localhost:8000/',
  LOGIN_URL: 'http://localhost:8000/accounts/login/',
  ASSOCIATES_LIST: [
    {'label': '<Associate>', 'value': ''},
    {'label': 'Emil', 'value': '1'},
    {'label': 'Dmitriy', 'value': '2'},
    {'label': 'Pearl', 'value': '4'},
    {'label': 'Ruth', 'value': '7'},
    {'label': 'Kurt', 'value': '13'},
    {'label': 'Audrey', 'value': '15'},
    // {'label': 'Becca', 'value': '18'},
    // {'label': 'Greg', 'value': '16'}
  ]
};



export const environment = {
  production: true,
  FURNICLOUD_URL: 'http://cloud.furnitalia.com/',
  LOGIN_URL: 'http://cloud.furnitalia.com/accounts/login/',
  ASSOCIATES_LIST: [
    {'label': '<Associate>', 'value': ''},
    {'label': 'Emil', 'value': '1'},
    {'label': 'Dmitriy', 'value': '2'},
    {'label': 'Pearl', 'value': '4'},
    {'label': 'Ruth', 'value': '7'},
    {'label': 'Kurt', 'value': '13'},
    {'label': 'Audrey', 'value': '15'},
    {'label': 'Greg', 'value': '16'},
    {'label': 'Becca', 'value': '18'}
  ]
};

import { TestBed, inject } from '@angular/core/testing';

import { IncomingShipmentService } from './incoming-shipment.service';

describe('IncomingShipmentService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [IncomingShipmentService]
    });
  });

  it('should be created', inject([IncomingShipmentService], (service: IncomingShipmentService) => {
    expect(service).toBeTruthy();
  }));
});

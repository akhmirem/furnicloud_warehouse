import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WarehouseReceivingComponent } from './warehouse-receiving.component';

describe('WarehouseReceivingComponent', () => {
  let component: WarehouseReceivingComponent;
  let fixture: ComponentFixture<WarehouseReceivingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WarehouseReceivingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WarehouseReceivingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import {Component, OnInit, ViewChild} from '@angular/core';
import {Attachment, FurniCloudItem, IncomingItem, IncomingShipment} from '../incoming-shipment';
import {IncomingShipmentService, VENDORS_LIST, ASSOCIATES_LIST} from '../incoming-shipment.service';
import {DataTable} from 'primeng/primeng';
import {Message} from 'primeng/api';
import {MessageService} from 'primeng/components/common/messageservice';
import {environment} from '../../environments/environment';
// import moment = require('moment');
import * as moment from 'moment';
import {HttpXsrfTokenExtractor} from '@angular/common/http';

@Component({
  selector: 'app-warehouse-receiving',
  templateUrl: './warehouse-receiving.component.html',
  styleUrls: ['./warehouse-receiving.component.css']
})
export class WarehouseReceivingComponent implements OnInit {

  incomingShipments: IncomingShipment[] = [];

  selectedItem: IncomingItem;
  incomingShipment: IncomingShipment = new IncomingShipment();
  newIncomingShipment: boolean;
  displayDialog: boolean;
  furnicloudItems: FurniCloudItem[];
  displayItemDialog: boolean;
  selectedSOItem: FurniCloudItem;
  VENDORS_LIST: any[] = VENDORS_LIST;
  ASSOCIATES_LIST: any[] = ASSOCIATES_LIST;
  displayItemDetailDialog: boolean;
  so_num = '';
  loading: boolean;
  @ViewChild('dt') datatable: DataTable;
  attachmentUploadUrl = '';

  uploadedFiles: any[] = [];

  attachmentURL = '';


  constructor(private incomingShipmentService: IncomingShipmentService,
              private messageService: MessageService,
              private tokenExtractor: HttpXsrfTokenExtractor) {
  }

  ngOnInit() {
    this.getIncomingShipments();
  }

  getIncomingShipments() {
    this.loading = true;
    this.incomingShipmentService.getIncomingShipments()
      .subscribe(shipments => {
          this.incomingShipments = shipments;
          console.log(this.incomingShipments);
          this.loading = false;
        },
        (err) => {
          this.loading = false;
          if (err.status === 403) {
            // proceed to login
            window.location.href = environment.LOGIN_URL;
          }
        }
      );
  }

  showDialogToAdd() {
    this.newIncomingShipment = true;
    this.incomingShipment = new IncomingShipment();
    this.incomingShipment.date = new Date();
    // this.displayDialog = true;

    this.incomingShipments = [this.incomingShipment, ...this.incomingShipments];
    this.incomingShipment = this.incomingShipments[this.findSelectedShipmentIndex()];

    this.datatable.toggleRow(this.incomingShipment);

  }

  saveShipment() {
    const incomingShipments = [...this.incomingShipments];

    const itemsToSave = this.incomingShipment.items;
    const tempShipment = this.incomingShipment;

    this.incomingShipmentService.saveShipment(this.incomingShipment)
      .subscribe(
        (savedShipment) => {

          console.log('Saved Shipment in component:');
          console.log(savedShipment);
          console.log('PK = ' + savedShipment.pk);

          this.incomingShipment = savedShipment;
          for (const item of itemsToSave) {
            item.shipment = this.incomingShipment.pk;
          }
          this.incomingShipment.items = itemsToSave;

          if (this.incomingShipment.items.length > 0) {
            console.log('Call to save items ...');
            this.incomingShipmentService.saveItems(this.incomingShipment).subscribe(
              (savedShipmentWithItems) => {

                this.incomingShipment = savedShipmentWithItems;
                incomingShipments[this.findSelectedShipmentIndex()] = this.incomingShipment;
                this.datatable.toggleRow(tempShipment);
                this.incomingShipments = incomingShipments;
                this.incomingShipment = this.incomingShipments[this.findSelectedShipmentIndex()];
                this.messageService.add(
                  {severity: 'success', summary: 'Shipment', detail: 'Shipment was saved!'});
              },
              (err) => {
                console.log('Error saving items attached to shipment!');
                this.messageService.add(
                  {severity: 'error', summary: 'Shipment', detail: 'Error saving shipment!'});
              }
            );
          } else {

            incomingShipments[this.findSelectedShipmentIndex()] = this.incomingShipment;
            this.datatable.toggleRow(tempShipment);
            this.incomingShipments = incomingShipments;
            this.incomingShipment = this.incomingShipments[this.findSelectedShipmentIndex()];
            this.messageService.add(
              {severity: 'success', summary: 'Shipment', detail: 'Shipment was saved'});
          }

        },
        (err) => console.log('Error saving shipment!')
      );
    this.displayDialog = false;
  }

  closeShipmentDialog() {
    this.datatable.toggleRow(this.incomingShipment);
  }

  deleteShipment() {

    const index = this.findSelectedShipmentIndex();
    if (this.incomingShipment.pk) {
      this.incomingShipmentService.deleteShipment(this.incomingShipment).subscribe(
        () => {
          this.incomingShipments = this.incomingShipments.filter((val, i) => i !== index);
          this.incomingShipment = null;
          this.messageService.add(
            {severity: 'success', summary: 'Shipment', detail: 'Shipment was deleted.'});
        },
        () => {
          console.log('Failed to delete shipment!');
          this.messageService.add(
            {severity: 'error', summary: 'Shipment', detail: 'Error deleting shipment!'});
        }
      );
    } else {
      this.incomingShipments = this.incomingShipments.filter((val, i) => i !== index);
      this.incomingShipment = null;
      this.messageService.add(
        {severity: 'success', summary: 'Shipment', detail: 'Shipment was deleted.'});
    }

  }

  onRowSelect(event) {
    this.newIncomingShipment = false;
    // this.incomingShipment = this.cloneShipment(event.data);
    // console.log(event.data);
    this.datatable.toggleRow(event.data);
    this.updateFileUploadUrl();
  }

  cloneShipment(c: IncomingShipment): IncomingShipment {
    const shipment = new IncomingShipment();
    for (const prop in c) {
      shipment[prop] = c[prop];
    }
    return shipment;
  }

  findSelectedShipmentIndex(): number {
    return this.incomingShipments.indexOf(this.incomingShipment);
  }

  findSelectedItemIndex(): number {
    return this.incomingShipment.items.indexOf(this.selectedItem);
  }

  addItem() {
    const incomingItems = [...this.incomingShipment.items];
    const item = new IncomingItem('');
    item.sale_item = false;
    incomingItems.push(item);
    this.incomingShipment.items = incomingItems;


    const incomingShipments = [...this.incomingShipments];
    incomingShipments[this.findSelectedShipmentIndex()] = this.incomingShipment;

    // this.incomingShipmentService.saveShipment(this.incomingShipment);

    this.incomingShipments = incomingShipments;

    this.selectedItem = item;
    // this.displayItemDialog = true;
  }

  saveItem(event) {
    if (this.selectedItem && this.selectedSOItem) {
      this.selectedItem.so_num = this.selectedSOItem.so_num;
      this.selectedItem.sale_item = true;
      this.selectedItem.order_id = this.selectedSOItem.order_id;
      this.selectedItem.item_id = this.selectedSOItem.pk;
      if (this.selectedItem.description.trim()) {
        this.selectedItem.description = this.selectedItem.description + '\n' + 'ItemId:' + this.selectedSOItem.pk;
      } else {
        this.selectedItem.description = this.selectedSOItem.description + ' | ItemId: ' + this.selectedSOItem.pk;
      }

      this.displayItemDialog = false;

    }
  }

  showOrderSearch(item: IncomingItem) {
    this.selectedItem = item;
    this.so_num = '';
    this.displayItemDialog = true;
  }

  lookupFurniCloudItems() {

    if (this.so_num.trim().length === 0) {
      return;
    }

    this.incomingShipmentService.findFurniCloudOrders(this.so_num).subscribe(
      (items: FurniCloudItem[]) => {
        this.furnicloudItems = items;
      },
      () => this.log('Failed to search for FurniCloud SO#!')
    );

    /*const testItems: FurniCloudItem[] = [
      {
        pk: 1,
        order_id: 1884,
        so_num: 'SO-30312',
        status: 'Pending',
        description: 'Sample Item',
        po_num: 'PO-30312',
        po_date: new Date('01/01/2018 08:00:00')
      },
      {
        pk: 2,
        order_id: 1884,
        so_num: 'SO-30312',
        status: 'Pending',
        description: 'Sample Item 2',
        po_num: 'PO-30312',
        po_date: new Date('01/02/2018 08:00:00')
      }
    ];

    // this.furnicloudOrders = [testOrder];
    this.furnicloudItems = testItems;*/
  }

  /*assignShipmentItems(event) {
    const incomingItems = [...this.incomingShipment.items];

    this.selectedSOItems.forEach(function (element) {
      console.log('item ' + element.description);
      let item = new IncomingItem(element.description);
      item.sale_item = true;
      item.so_num = this.selectedSO.number;
      item.order_id = this.selectedSO.pk;
      item.item_id = element.pk;
      incomingItems.push(item);
    });
    this.incomingShipment.items = incomingItems;


    this.displayItemDialog = false;
    this.displayOrderDialog = false;
    this.furnicloudOrders = [];
    // this.selectedSO = null;
    this.selectedSOItems = [];
  }*/

  getAssociateName(received_by: string): string {
    const associates = this.ASSOCIATES_LIST.filter(a => a.value === received_by.toString());

    if (associates.length === 0) {
      return '';
    }

    return associates[0].label;
  }

  showItemDetails(item: any) {
    this.selectedItem = item;
    this.displayItemDetailDialog = true;
  }

  closeItemDetailDialog() {
    this.displayItemDetailDialog = false;
    this.selectedItem = null;
  }

  deleteItem() {
    console.log('Begin deleting item ...');
    const itemIndex = this.findSelectedItemIndex();

    if (this.selectedItem.pk) {
      this.incomingShipmentService.deleteItem(this.selectedItem).subscribe(
        () => {
          this.incomingShipment.items = this.incomingShipment.items.filter((val, i) => i !== itemIndex);
          this.selectedItem = null;
          this.messageService.add(
            {severity: 'success', summary: 'Item', detail: 'Item was deleted.'});
        },
        () => {
          console.log('Failed to delete item');
          this.messageService.add(
            {severity: 'error', summary: 'Item', detail: 'Error deleting item!'});
        }
      );
    } else {
      this.incomingShipment.items = this.incomingShipment.items.filter((val, i) => i !== itemIndex);
      this.selectedItem = null;
      this.messageService.add(
        {severity: 'success', summary: 'Item', detail: 'Item was deleted'});
    }

    this.displayItemDetailDialog = false;
    console.log('After deleting item...');
  }

  private log(s: any) {
    console.log(s);
  }

  onUpload(event) {
    for (const file of event.files) {
      this.uploadedFiles.push(file);
    }

    const xhr: XMLHttpRequest = event.xhr;
    console.log(xhr.response);

    if (xhr.status === 201) {
      // update shipment attachments with newly created file

      const attachments = [...this.incomingShipment.files];
      const attachment: Attachment = <Attachment> JSON.parse(xhr.response);
      attachments.push(attachment);
      this.incomingShipment.files = attachments;


      const incomingShipments = [...this.incomingShipments];
      incomingShipments[this.findSelectedShipmentIndex()] = this.incomingShipment;
      this.incomingShipments = incomingShipments;

    }

    this.messageService.add(
      {severity: 'info', summary: 'File Uploaded', detail: ''});
  }

  onBeforeUpload(event) {
    const xhr: XMLHttpRequest = event.xhr;
    const formData: FormData = event.formData;
    xhr.setRequestHeader('X-CSRFToken', this.tokenExtractor.getToken());
    formData.append('shipment', this.incomingShipment.pk.toString());
    formData.append('description', '');
  }

  getFileName(url: string): string {
    let fileName = '';
    if (url) {
      const parts = url.split('/');
      if (parts.length > 0) {
        fileName = parts.pop();
      }
    }
    return fileName;
  }

  private updateFileUploadUrl() {
    this.attachmentUploadUrl = this.incomingShipmentService.furnicloudURL
      + 'api/warehouse/'
      + this.incomingShipment.pk
      + /files/;
  }

  displayAttachmentDialog($event, attachment: Attachment) {
    if (! attachment.file.endsWith('.pdf')) {
      this.attachmentURL = attachment.file;
      $event.preventDefault();
    }
  }
}

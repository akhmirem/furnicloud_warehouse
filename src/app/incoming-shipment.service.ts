///<reference path="incoming-shipment.ts"/>
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {of} from 'rxjs/observable/of';
import {catchError, map, tap} from 'rxjs/operators';
import {FurniCloudItem, FurniCloudOrder, IncomingItem, IncomingShipment} from './incoming-shipment';
// import {INCOMING_SHIPMENTS} from './mock-incoming-shipments';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {Observer} from 'rxjs/Observer';
import {SelectItem} from 'primeng/primeng';
import {environment} from '../environments/environment';
import * as moment from 'moment';
// import moment = require('moment');

@Injectable()
export class IncomingShipmentService {

  furnicloudURL: string;
  private shipmentsUrl: string;
  private itemsUrl: string;
  private furniCloudSOItemsUrl: string;
  private httpOptions: any = {
    headers: new HttpHeaders().set('Content-Type', 'application/json'),
    withCredentials: true
  };

  constructor(private http: HttpClient) {
    this.furnicloudURL = environment.FURNICLOUD_URL;
    this.shipmentsUrl = this.furnicloudURL + 'api/warehouse/';
    this.itemsUrl = this.furnicloudURL + 'api/incoming-items/';
    this.furniCloudSOItemsUrl = this.furnicloudURL + 'api/so_items/';
  }

  getIncomingShipments(): Observable<any> {
    // return of(INCOMING_SHIPMENTS);

    return this.http.get(this.shipmentsUrl, this.httpOptions)
      .pipe(
        map((shipments: any) => {
          const incomingShipments = shipments.results.map(s => {
            s.date = new Date(s.date); // moment(s.date).local();
            // s.date = new Date(moment(s.date).format('YYYY-MM-DD HH:mm'));

            return s;
          });
          return incomingShipments;
        })
      );
  }

  saveShipment(shipment: IncomingShipment): Observable<any> {

    if (shipment.pk) {
      console.log('updating shipment w/id= ' + shipment.pk + '...');
      return this.http.put<any>(this.shipmentsUrl + shipment.pk + '/', shipment, this.httpOptions)
        .pipe(
          tap((savedShipment: any) => {
            this.log(`updated shipment: pk=${savedShipment.pk}`);
            savedShipment.date = new Date(savedShipment.date);
            this.log(savedShipment);
          }),
          catchError(this.handleError<any>('saveShipment'))
        );

    } else {

      console.log('saving shipment ...');
      console.log('Date before');
      console.log(shipment.date);
      // shipment.date = moment(shipment.date).toISOString();
      return this.http.post<IncomingShipment>(this.shipmentsUrl, shipment, this.httpOptions)
        .pipe(
          tap((savedShipment: any) => {
            this.log(`created shipment`);
            console.log('Date raw:');
            console.log(savedShipment.date);
            savedShipment.date = new Date(savedShipment.date);
            console.log('Date after:');
            console.log(savedShipment.date);
            this.log(savedShipment);
          }),
          catchError(this.handleError<any>('saveShipment'))
        );
    }

  }

  deleteShipment(shipment: IncomingShipment) {
    const url = this.shipmentsUrl + shipment.pk + '/';
    return this.http.delete<IncomingShipment>(url, this.httpOptions).pipe(
      tap(_ => this.log(`deleted shipment w/id=${shipment.pk}`)),
      catchError(this.handleError<any>('deleteShipment'))
    );
  }


  saveItem(shipmentItem: IncomingItem): Observable<any> {
    console.log('saving item ' + shipmentItem.description + ' ...');
    if (shipmentItem.pk) {
      return this.http.put<any>(this.itemsUrl + shipmentItem.pk + '/', shipmentItem, this.httpOptions)
        .pipe(
          tap((savedItem: any) => {
            this.log(`updated item`);
            this.log(savedItem);
          }),
          catchError(this.handleError<any>('saveItem'))
        );
    } else {
      return this.http.post<IncomingItem>(this.itemsUrl, shipmentItem, this.httpOptions)
        .pipe(
          tap((savedItem: any) => {
            this.log(`created item`);
            this.log(savedItem);
          }),
          catchError(this.handleError<any>('saveItem'))
        );
    }
  }

  saveItems(incomingShipment: IncomingShipment): Observable<any> {

    const observable = Observable.create((observer: Observer<IncomingShipment>) => {

      const savedShipmentItems: IncomingItem[] = [];

      for (const item of incomingShipment.items) {
        this.saveItem(item).subscribe(
          (savedItem) => {
            this.log('Item ' + savedItem.description + ' was saved.');
            savedShipmentItems.push(savedItem);
          },
          (err) => {
            this.log('Error saving item: ' + item.description);
            observer.error('Error saving incoming shipment items');
          }
        );
      }

      console.log('# of items saved: ' + savedShipmentItems.length);
      incomingShipment.items = savedShipmentItems;
      observer.next(incomingShipment);

    });

    return observable;
  }

  deleteItem(item: IncomingItem) {
    const url = this.itemsUrl + item.pk + '/';
    return this.http.delete<IncomingItem>(url, this.httpOptions).pipe(
      tap(_ => this.log(`deleted item w/id=${item.pk}`)),
      catchError(this.handleError<any>('deleteItem'))
    );
  }

  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  private log(message: any) {
    console.log(message);
  }

  findFurniCloudOrders(so_num: string): Observable<FurniCloudItem[]> {
    const options = {...this.httpOptions};
    options.params = new HttpParams().set('so_num', so_num);

    return this.http.get(this.furniCloudSOItemsUrl, options)
      .pipe(map((returnedItems: any) => {
          return returnedItems.results.map(i => {
              if (i.po_date) {
                i.po_date = new Date(i.po_date);
              }
              return i;
            }
          );
        }
      ));
  }
}

export const VENDORS_LIST = [
  {'label': '<Vendor>', 'value': ''},
  {'label': 'Natuzzi Italia', 'value': 'Natuzzi Italia'},
  {'label': 'Natuzzi Editions', 'value': 'Natuzzi Editions'},
  {'label': 'Natuzzi Re-Vive', 'value': 'Natuzzi Re-Vive'},
  {'label': 'ALF Italia', 'value': 'ALF Italia'},
  {'label': 'Bontempi', 'value': 'Bontempi'},
  {'label': 'Stressless', 'value': 'Stressless'},
  {'label': 'Cattelan', 'value': 'Cattelan'},
  {'label': 'Bauformat', 'value': 'Bauformat'},
  {'label': 'Miele', 'value': 'Miele'}
];

export const ASSOCIATES_LIST: SelectItem[] = environment.ASSOCIATES_LIST;


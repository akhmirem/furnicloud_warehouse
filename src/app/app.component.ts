import {Component, ViewEncapsulation} from '@angular/core';
import { environment } from '../environments/environment';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class AppComponent {
  title = 'FurniCloud Incoming Shipments';
  furnicloudURL: string = environment.FURNICLOUD_URL;
}

import {IncomingItem, IncomingShipment} from './incoming-shipment';

/*const po1 = new PurchasedOrder();
po1.poNum = 'PO1';
po1.soNum = 'SO1';
po1.items = [new IncomingItem('Sofa'), new IncomingItem('Armchair')];

const po2 = new PurchasedOrder();
po2.poNum = 'PO124';
po2.soNum = 'SO124';
po2.items = [new IncomingItem('Table'), new IncomingItem('Chair white'), new IncomingItem('Chair Black')];
*/

export const INCOMING_SHIPMENTS: IncomingShipment[] = [
  {
    'pk': 1,
    'date': new Date('01/06/2018 13:45:56'),
    'vendor': 'Natuzzi Italia',
    'num_boxes': 1,
    'received_by': '7',
    'notes': 'test',
    'items': [
      {
        'pk': 1,
        'shipment': 1,
        'po_num': 'PO30312',
        'so_num': 'SO-30312',
        'description': 'Incoming item 1',
        'order': {
          'pk': 1884,
          'number': 'SO-30312',
          'order_date': new Date('01/01/2018 08:00:00'),
          'customer': 'Emil Akhmirov',
          'status': 'Pending',
          'store': 'Roseville'
        },
        'order_id': 1884,
        'item': {
          'pk': 2494,
          'status': 'Pending',
          'description': '5821 Avana Sofa',
          'po_num': 'PO30312',
          'po_date': '2018-01-11',
          'ack_num': 'ACK30312',
          'ack_date': '2018-01-11'
        },
        'item_id': 2494
      },
      {
        'pk': 2,
        'shipment': 1,
        'po_num': 'PO30312',
        'so_num': 'SO-30312',
        'description': 'Incoming item 1',
        'order': {
          'pk': 1884,
          'number': 'SO-30312',
          'order_date': new Date('01/01/2018 08:00:00'),
          'customer': 'Emil Akhmirov',
          'status': 'Pending',
          'store': 'Roseville'
        },
        'order_id': 1884,
        'item': {
          'pk': 2495,
          'status': 'Pending',
          'description': 'Item 2',
          'po_num': 'PO30312',
          'po_date': '2018-01-11',
          'ack_num': 'ACK30312',
          'ack_date': '2018-01-11'
        },
        'item_id': 2495
      }
    ],
    'files': []
  },
  {
    'pk': 2,
    'date': new Date('01/07/2018 13:45:56'),
    'vendor': 'Natuzzi Italia',
    'num_boxes': 1,
    'received_by': '7',
    'notes': 'test 2',
    'items': [
      {
        'pk': 3,
        'shipment': 2,
        'po_num': 'PO30311',
        'so_num': 'SO-30311',
        'description': 'Incoming item 1',
        'order': {
          'pk': 1883,
          'number': 'SO-30311',
          'order_date': new Date('01/03/2018 08:00:00'),
          'customer': 'Emil Akhmirov',
          'status': 'Pending',
          'store': 'Roseville'
        },
        'order_id': 1883,
        'item': {
          'pk': 2497,
          'status': 'Pending',
          'description': 'Item 1',
          'po_num': 'PO30311',
          'po_date': '2018-01-11',
          'ack_num': 'ACK30311',
          'ack_date': '2018-01-11'
        },
        'item_id': 2497
      },
      {
        'pk': 4,
        'shipment': 2,
        'po_num': 'PO30311',
        'so_num': 'SO-30311',
        'description': 'Incoming item 1',
        'order': {
          'pk': 1883,
          'number': 'SO-30311',
          'order_date': new Date('01/03/2018 08:00:00'),
          'customer': 'Emil Akhmirov',
          'status': 'Pending',
          'store': 'Roseville'
        },
        'order_id': 1883,
        'item': {
          'pk': 2498,
          'status': 'Pending',
          'description': 'Item 2',
          'po_num': 'PO30311',
          'po_date': '2018-01-11',
          'ack_num': 'ACK30311',
          'ack_date': '2018-01-11'
        },
        'item_id': 2498
      }
    ],
    'files': []
  }
];


export class FurniCloudOrder {
  pk: number;
  number: string;
  order_date: Date;
  customer: string;
  status: string;
  store: string;
  items?: FurniCloudItem[];
}

export class FurniCloudItem {
  pk: number;
  order_id: number;
  so_num: string;
  status: string;
  description: string;
  po_num: string;
  po_date: Date;
}

export class IncomingItem {
  pk?: number;
  shipment?: number;
  po_num: string;
  so_num?: string;
  description?: string;
  order?: any;
  order_id?: number;
  item?: any;
  item_id?: number;
  sale_item?: boolean;
  constructor(description: string) {
    this.description = description;
    this.sale_item = false;
  }
}

export class Attachment {
  pk?: number;
  shipment?: number;
  file?: any;
  description?: string;
  created?: Date;
  constructor() {
    this.description = '';
    this.created = new Date();
  }
}

export class IncomingShipment {
  pk?: number;
  date: Date;
  vendor: string;
  received_by: string;
  num_boxes: number;
  notes: string;
  items: IncomingItem[];
  files: Attachment[];
  constructor() {
    this.date = new Date();
    this.vendor = '';
    this.received_by = '';
    this.num_boxes = 0;
    this.notes = '';
    this.items = [];
    this.files = [];
  }
}

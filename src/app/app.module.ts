import { BrowserModule } from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HTTP_INTERCEPTORS, HttpClientModule, HttpClientXsrfModule} from '@angular/common/http';
import { InputTextModule, DataTableModule, ButtonModule, DialogModule, DropdownModule, CalendarModule,
  OverlayPanelModule, SidebarModule } from 'primeng/primeng';
import {FileUploadModule} from 'primeng/fileupload';
import {GrowlModule} from 'primeng/growl';
import { AppComponent } from './app.component';
import { WarehouseReceivingComponent } from './warehouse-receiving/warehouse-receiving.component';
import {IncomingShipmentService} from './incoming-shipment.service';
import {HttpXsrfInterceptor} from './http-interceptors';
import {MessageService} from 'primeng/components/common/messageservice';


@NgModule({
  declarations: [
    AppComponent,
    WarehouseReceivingComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    HttpClientXsrfModule.withOptions({
      cookieName: 'csrftoken',
      headerName: 'x-csrftoken'
    }),
    // HttpClientInMemoryWebApiModule.forRoot(
    //   InMemoryDataService, {dataEncapsulation: false}
    // ),
    DataTableModule,
    InputTextModule,
    ButtonModule,
    DialogModule,
    DropdownModule,
    CalendarModule,
    OverlayPanelModule,
    SidebarModule,
    GrowlModule,
    FileUploadModule
  ],
  providers: [
    IncomingShipmentService,
    MessageService,
    { provide: HTTP_INTERCEPTORS, useClass: HttpXsrfInterceptor, multi: true }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
